<?php
require_once("Animal.php");
require_once("Frog.php");
require_once("Ape.php");


$sheep = new Animal("shaun");
echo "Nama Hewan : $sheep->name <br>"; 
echo "Jumlah Kaki : $sheep->legs <br>"; 
echo "Berdarah Dingin : $sheep->cold_blood <br>";

echo "<br>";

$kodok = new Frog ("buduk");
echo "Nama Hewan : $kodok->name <br>";
echo "Suara Lompat : $kodok->jump <br>";

echo "<br>";

$sungokong = new Ape("kera sakti");
echo "Nama Hewan : $sungokong->name <br>";
echo "Suara Teriakan : $sungokong->yell <br>";


?>